## My open source contributions

#### Improving the Terraform training code base:
https://github.com/hashicorp/learn-terraform-enforce-policies/pull/11

https://github.com/hashicorp/learn-terraform-functions/pull/6

#### Add functionality to bulk create/delete OpenStack Security Group rules via Ansible:
https://opendev.org/openstack/ansible-collections-openstack/commit/124e174d27cd0befd50f3cf2bc6f89f35c4151ef
